package main

import (
	"fmt"
	"log"
	"net/http"
)

func handlerApiHello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello, 世界, version 4. Handling /any")
	fmt.Println("Handling /any")
	logRequestHeaders(r)
}

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "Hello, 世界, version 4. Handling: /")
	fmt.Println("Handling /")
	logRequestHeaders(r)
}

func logRequestHeaders(r *http.Request) {
    for name, values := range r.Header {
        for _, value := range values {
            fmt.Println("  ", name, value)
        }
    }
}

func main() {
	http.HandleFunc("/any", handlerApiHello)
	http.HandleFunc("/", handler)
	fmt.Println("Running demo app. Press Ctrl+C to exit...")
	log.Fatal(http.ListenAndServe(":8888", nil))
}